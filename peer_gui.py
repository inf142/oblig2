import socket
import threading
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QVBoxLayout, QLabel, QWidget, QMenu, QAction
from PyQt5.QtCore import Qt, QMimeData, QPoint, QRectF
from PyQt5.QtGui import QDragEnterEvent, QDragMoveEvent, QDropEvent, QLinearGradient, QPainter, QPalette, QBrush, QColor, QPainterPath, QPen, QFont, QPixmap 

file_sharing_registery = {}

class Peer():
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('localhost', 42))

    def send_file(self):
        pass

    def recive_file(self):
        pass

    def add_file(self):
        pass

def main():
    pass

class FileDropBox(QWidget):
    def __init__(self, parent=None):
        super(FileDropBox, self).__init__(parent)
        self.setAcceptDrops(True)
        self.setFixedSize(300, 200)
        palette = QPalette()

        image_label = QLabel(self)
        image_pixmap = QPixmap("addSymbol.png")
        image_pixmap = image_pixmap.scaled(60, 60, Qt.KeepAspectRatio, Qt.SmoothTransformation)
        image_label.setPixmap(image_pixmap)

        image_label.setGeometry(
            int((self.width() - image_pixmap.width()) / 2),
           100,
            image_pixmap.width(), image_pixmap.height()
        )
        image_label.setStyleSheet("background-color: transparent;")

        self.label = QLabel("Add your files", self)
        palette.setColor(QPalette.Foreground, QColor("white"))
        self.label.setPalette(palette)
        self.label.setAlignment(Qt.AlignCenter)
        label_width = 200
        label_height = 50
        label_x = int((self.width() - label_width) / 2)
        self.label.setGeometry(label_x, 40, label_width, label_height)
        self.label.setStyleSheet("background-color: transparent;")
        self.label.setAttribute(Qt.WA_TransparentForMouseEvents)
        font = QFont()
        font.setPointSize(14)
        font.setBold(True)
        self.label.setFont(font)
        self.setStyleSheet("border: 0px;")



    def dragEnterEvent(self, event: QDragEnterEvent):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()

    def paintEvent(self, event):
        gradient = QLinearGradient(QPoint(0, 0), QPoint(self.width(), self.height()))
        gradient.setColorAt(0, QColor("#808692"))
        gradient.setColorAt(1, QColor("#737e8d"))

        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.setBrush(QBrush(gradient))
        painter.setPen(Qt.NoPen)
        path = QPainterPath()
        radius = 40

        path.addRoundedRect(QRectF(0,0,300,200), radius, radius)
        painter.drawPath(path)

    def dragMoveEvent(self, event: QDragMoveEvent):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
    def dropEvent(self, event: QDropEvent):
        if event.mimeData().hasUrls():
            event.acceptProposedAction()
            file_urls = [url.toLocalFile() for url in event.mimeData().urls()]
            self.handle_dropped_files(file_urls)

    def handle_dropped_files(self, files):
        for file in files:
            print(f"Dropped file: {file}")

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def setUpImage(self):
        label = QLabel(self)
        pixmap = QPixmap("logo.png")
        label.setPixmap(pixmap)
        label.setStyleSheet("background-color: transparent;")
        return label

    def initUI(self):
        menubar = self.menuBar()
        menubar.setStyleSheet("""
            QMenuBar{
                background-color : #8bb7a1;
                border-radius: 10px;
            }
            QMenuBar::Item{
                color : #282d33 ;
            }
            """) 
        menubar.setFixedWidth(60);

        peer_menu = menubar.addMenu("Peer's")
        peer_menu.setStyleSheet("background-color : #8bb7a1")
        new_action = QAction("new", self)
        peer_menu.addAction(new_action)
        sub_menu = QMenu("files", self)
        sub_menu.setStyleSheet("background : #8bb7a1")
        download_file = QAction("download_file", self)
        sub_menu.addAction(download_file)
        peer_menu.addMenu(sub_menu)

        exit_action = QAction("Exit", self)
        exit_action.triggered.connect(self.close)

        self.setStyleSheet("QWidget {background-color: qlineargradient(y1: 0, y2: 1, stop: 0 #282d33, stop: 1 #636c79)}")

        self.setGeometry(0, 0, 600, 600)
        self.setWindowTitle("Peer to Peer file sharing")

        # Create a central widget and QVBoxLayout
        central_widget = QWidget(self)
        self.setCentralWidget(central_widget)
        main_layout = QVBoxLayout(central_widget)

        # Add Image, Peer's menu, and FileDropBox to the layout
        main_layout.addWidget(self.setUpImage(), alignment=Qt.AlignHCenter)
        main_layout.addStretch(1)
        main_layout.addWidget(FileDropBox(), alignment=Qt.AlignHCenter)
        main_layout.addStretch(1)

        self.show()



def main_window():
    app = QApplication(sys.argv)
    main_win = MainWindow()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main_window()

#TODO  
'''
Tror enkleste løsning e at alle peers lage sitt eget directory der du kan last opp og last ned fila.

'''