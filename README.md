# INF142 obligatory 2

## Installations
For installing dependencies run:  
pip install -r requirements.txt

## How to run
PS! Before running peers, make sure the peers have each their "Files" folder in which the files that should be downloadable are.

- Run the tracker first in its own terminal window: 'python tracker.py'
- On new terminal windows, run as many peers as wanted: 'python peer.py'
- On a peer, press enter to see the list of available files and to choose which one to download
- To disconnect a peer from the tracker, you can either keyboard interrupt (ctrl+c / cmd+.) or just type a whitespace and press enter.

## Implementation
We've used the given skeleton code for both tracker and peer as a baseline for this assignment and done all the TODOs in each skeleton code.

### Explanation of code
In this section we will describe the code that we've written for the TODOs in each of the skeleton code. We'll do this sequentually based on where the code lies in the file.

#### Tracker
In the tracker we've done what the TODOs tells us to do in the read and accept function. 
In the read function we first check if we receive data from the connected peer. If so, we retreive the peer name, in other words, its address using getpeername function. After that we serve the peer and send the reply serve function gives us. If we do not receive data from the connected peer, we unregister it using selector and close the connection socket.

In the accept function, we do as the TODOs says, we first accept the connection of a peer using sock.accept() to retrieve the conn variable, then we set its blocking to False using conn.setBlocking(False), and then registering to the selector.

This concludes all the TODOs in tracker, but in addition to this, we've added something to the serve function. In the case of "ADD", we've added an if-statement to check if the file doesn't start with ".", which makes it so that it doesn't add hidden files and folders (This was wanted due to the .git folder as we're using git which we didn't want to add).

#### Peer
First in peer, we go through the remote_call function. We first send the message to the socket using sock.sendall(message), next we receive the response we get back from it, to then return that response.

Next function is read_peer. Here we essentially do the same as in the read function in the tracker. The differences are that here we don't get the peer name, and all of it is within a try block, as we had to make some error handling exceptions.

accept_peer is identical to accept in tracker, only difference is that the function that the conn is being registered with is read_peer and not read.

serve_peer_thread is next, and here we had to make it so that each peer got their own ip-address, as an error that complained about it occured here. So first we set the variables server to None and port to 12005 as well as ip_address to PEER (which is the ip-address of the peer). Next, we do a while True loop, where we try to create a server with the address, and break if that works. If an OSError occurs complaining about identical ip-addresses, we increment the ip_address variable by 1, so for example 127.0.0.1 becomes 127.0.0.2 and so on. This is done with a function called increment_ip which we will come back to after this function. After the ip_address has been incremented, we continue on with the loop and try to create the server again but with the new incremented ip_address, and after that we set the blocking to False and registers it using selector.

increment_ip is the function mentioned earlier that is used to increment the ip-address when creating a server if identical ip-addresses occurs there. It utilizes the ipaddress library. First it converts the given ip-address (in str) to an IPv4Address object, to which we can just increment by 1 to get our desired ip-address increment. We then convert it back to a string and return it.

add_from_peer is just used to call on the remote_call function with the tracker_sock and the file signaling the tracker to ADD it.

Next is list_files which was mostly written from before but where we had to fill the None's. First None was filled what was described in the TODO, which was "remote_call(tracker_sock, b"LIST_FILES").split(b" ", 1)". The second None we decode the data using utf-8 decoding and we split it. Last None, we use listdir(FOLDER) to list the local files.

Next function with TODOs is get_peer. First we retrieve the response of the remote_call. We then split it such that we can match the response with b'OK' and b'BAD' as desired. If the case is b'OK', we print out 'OK' and return the ip-address. If the case is b'BAD', we print out 'BAD' and return an empty string as described in the documentation of the function.

Lastly we made added one thing in the main function. Since the files names are being received with additional semicolons, when we try to download a file, we get error BAD and the file doesn't get downloaded. So to evade this problem, we just go through all the available files and replace the semicolons with empty strings.

## Link to Git repo:
https://git.app.uib.no/inf142/oblig2
