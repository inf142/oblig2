from collections import defaultdict
from os import environ
from random import choice
from selectors import EVENT_READ, DefaultSelector
from socket import create_server, socket

TRACKER = environ.get("TRACKER", "localhost")

tracker: defaultdict[bytes, set[str]] = defaultdict(set)
selector = DefaultSelector()

def serve(peer: str, data: bytes) -> bytes:
    match data.split(b" ", 1):
        case [b"ADD", file]:
            if not file.startswith(b"."):  # Ignore hidden files and folders
                tracker[file].add(peer)
                reply = b"OK "
                print(f"added {file} from {peer}")
            else:
                reply = b"BAD Hidden files or folders not allowed"
        case [b"REMOVE", file]:
            try:
                tracker[file].remove(peer)
            except ValueError:
                reply = b"BAD File does not exist"
            else:
                reply = b"OK "
        case [b"GET_PEER", file]:
            if peer_set := tracker.get(file):
                ip_address = choice(tuple(peer_set))[0]
                reply = b"OK " + ip_address.encode()
            else:
                reply = b"BAD File does not exist"
        case [b"LIST_FILES"]:
            print(b" ".join(tracker))
            reply = b"OK " + b"; ".join(tracker) + b";"
        case _:
            reply = b"BAD Method not supported"
    return reply

def read(conn: socket) -> None:
    # If we recv something from the socket
    #     Get peer from conn.getpeername()
    #     Serve connection
    # else:
    #     unregister connection
    #     close socket

    if (recv_data := conn.recv(1024)):
        peer = conn.getpeername()
        conn.sendall(serve(peer, recv_data))
    else:
        selector.unregister(conn)
        conn.close()

def accept(sock: socket) -> None:
    # Accept socket
    # Unblock conn
    # Register socket (conn, EVENT_READ, read)

    conn, _ = sock.accept()
    conn.setblocking(False)
    selector.register(conn, EVENT_READ, read)

def main() -> None:
    sock = create_server((TRACKER, 12000))
    sock.setblocking(False)
    selector.register(sock, EVENT_READ, accept)

    while True:
        events = selector.select()
        for key, _ in events:
            key.data(key.fileobj)

if __name__ == "__main__":
    main()